$(function() {
  var itemStore = new ItemStore();

  $.validator.addMethod(
    "uniqueItem",
    function(value, element) {
      return itemStore.itemExist(value);
    },
    "Item exists"
  );

  $('#item-form').validate({
    onsubmit: false,
    rules: {
      item: {
        required: true,
        uniqueItem: true
      }
    },
    messages: {
      item: {
        required: "Please enter item",
        uniqueUserName: "Item exists"
      }
    },
    submitHandler: function(form) {
      debugger
      var errors = validator.numberOfInvalids();
      if (errors) {
        var message = errors == 1
          ? 'You missed 1 field. It has been highlighted'
          : 'You missed ' + errors + ' fields. They have been highlighted';
        $("div.error span").html(message);
        $("div.error").show();
      } else {
        form.submit();
      }
    }
  });

  var defaultItem = function() {
    var saveItem = itemStore.getSavedItems();
    if(saveItem.length) {
      $.each(saveItem, function (index, value) {
        renderItem(index, value);
      });
    } else {
      renderEmpty();
    }
  };

  var renderItem = function(index, value) {
    index += 1;
    $('.container').append(`<div class="container--item" id="${index}-item"></div>`);
    $(`.container--item#${index}-item`).append(`<div class="container--item--index">${index})</div>`);
    $(`.container--item#${index}-item`).append(`<div class="container--item--value">${value}</div>`);
    $(`.container--item#${index}-item`).append(`<button data-index="${index}" class="container--item--remove">Remove</button>`);
  };

  var renderEmpty = function() {
    $('.container').append('<div class="empty-result">Not result</div>');
  };

  $("#item-form").submit(function( event ) {
    if($(this).valid()) {
      var item;
      item = $("#item-form .item-form--text").val();
      $("#item-form .item-form--text").val('');
      itemStore.addItem(item);
      $('.empty-result').remove();
      renderItem(itemStore.getItemIndex(item), item);
    }
    event.preventDefault();
  });

  $(document).on("click", ".container--item--remove" , function() {
    var data = $(this).data()['index'];
    $('.container').empty();
    itemStore.deleteItem(data);
    defaultItem();
  });

  defaultItem();

});