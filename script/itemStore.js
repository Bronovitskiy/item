function ItemStore() {
  this.items = [];

  this.addItem = function(item) {
    this.items.push(item);
    save(this.items)
  };

  this.getSavedItems = function() {
    var items = all();
    this.items = items ? items : [];
    return(this.items);
  };

  this.getItemIndex = function(item) {
    return(this.items.length - 1);
  };

  this.deleteItem = function(index) {
    this.items.splice(index - 1,1);
    save(this.items)
  };

  this.itemExist = function(item) {
    return(!~this.items.indexOf(item))
  };

  // private

  var all = function() {
    var items = $.cookie('items');
    return(items ? JSON.parse(items) : items);
  };

  var save = function(items) {
    var jsonItems = JSON.stringify(items);
    $.cookie('items', jsonItems);
  };
};